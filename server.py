import socket,threading
port = 53
ip = "0.0.0.0"

sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
sock.bind((ip,port))
sock.setblocking(False)
sock.settimeout(0.1)

def getQuestionDomain(data):

     domainData = data[12:]
     domain = ""
     extension = ""

     # print(domainData)
     lenght = 0
     readState = 0
     counter = 0
     totalReaded = 0 #should be 15 for www.diffea.com
     parts = []
     for byte in domainData:


             if  readState == 1:

                 if counter == lenght:
                     parts.append(domain)
                     lenght = 0
                     readState = 0
                 counter += 1
                 totalReaded += 1
                 domain += chr(byte)

                 if byte == 0:
                     break
             if readState == 0 and lenght == 0:
                 readState = 1
                 lenght = byte
                 counter = 0
                 # totalReaded += 1
                 domain = ''

                 # print("len: "+str(lenght))

     questionType = domainData[totalReaded:totalReaded+3]
     # print(questionType) #shouldbe 0x00 0x01

     # print("total readed byte:"+str(totalReaded))

     # print(parts)
     return (parts,questionType)



def getFlags(data):
    flags = bytes(data[2:4])

    qr = '1'
    opcode = flags[1:5]
    aa = '1'
    tc = '0'
    rd = '0'
    ra = '0'
    z = '000'
    rcpde = '0000'
    opcodeString = ""


    for bit in range(1,5):
        opcodeString+=str(ord(opcode)&(1<<bit))

    return int(qr+opcodeString+aa+tc+rd,2).to_bytes(1,byteorder='big') + int(ra+z+rcpde,2).to_bytes(1,byteorder='big')

def domainCombine(domainParts):
        domain = ""

        domain = ".".join([str(x) for x in domainParts])

        # for part in domainParts:
        #
        #         for char in part:
        #             domain+=char
        #         domain+="."

        return domain



def getPoisonedIp(domain):

        try:
            with open("poison_list.txt", "r+") as f:
                content = f.readlines()

            for line in content:

                poisonedDomain, getPoisonedIp = line.split(" ")

                if (poisonedDomain) == domain:

                    print("domain poisoned: ("+poisonedDomain+") - IP: "+getPoisonedIp)
                    # print("posionedDomain " + poisonedDomain)
                    # print("getPoisonedIp " + getPoisonedIp)
                    return getPoisonedIp
            return ""
        except:
            return ""






def buildResponse(data):

    # transaction ID
    id = data[0:2] #first line
    transactionId = id[0:2]
    tid = ""
    for byte in transactionId:
        tid += hex(byte)[2:] # let assume its 0x08  its actually 2byte array and if i remove 0x, i ll get raw 08 so i can get real tid
    # print(data)
    #flags
    flags = getFlags(data)
    # print(flags)
    #flags

    qdcount = b'\x00\x01' #how many domain queried?
    ancount = b'\x00\x01' #how many answer do you have?
    nscount = b'\x00\x00' #no idea
    arcount = b'\x00\x00' #same here no idea but probably find out soon

    dnsHeader = transactionId+flags+qdcount+ancount+nscount+arcount
    dnsBody = b''

    domainParts,questionType = getQuestionDomain(data)
    domainFull = domainCombine(domainParts)
    qbytes = dnsQuestionDomain(domainParts,'')
    poisonedIp = getPoisonedIp(domainFull)

    if poisonedIp != "":
        record = buildRecord(qbytes,poisonedIp)
    else:
        try:
            print("requested domain to find: "+domainFull)
            record = buildRecord(qbytes, socket.gethostbyname(domainFull))

        except:
            qdcount = b'\x00\x00'
            ancount = b'\x00\x00'  # how many answer do you have?
            print("couldnt resolve domain: "+domainFull)
            record = bytes([])

    # record = buildRecord(qbytes,"192.168.1.1")
    # record1 = buildRecord(qbytes,"192.168.1.2")

    dnsBody = record

    return dnsHeader + qbytes + dnsBody
    #
    #     #print(hex(byte))
    # print(tid)


def buildRecord(domainName,ip):

    recordByte = b'\xc0\x0c'
    # recordByte = b''

    recordByte = recordByte + bytes([0]) + bytes([1]) #type
    recordByte = recordByte + bytes([0]) + bytes([1]) #class

    recordByte += int(0).to_bytes(4,byteorder="big") #ttl

    recordByte = recordByte + bytes([0]) + bytes([4]) #32bit ip lenght 0x04

    for part in ip.split("."):
            recordByte += bytes([int(part)])

    return recordByte

def dnsQuestionDomain(parts,recordType):
        dnsQuestionDomain = b''
        # print(parts)
        for domainPart in parts:
            dnsQuestionDomain += (bytes([len(domainPart)]))

            for partByte in domainPart:
                dnsQuestionDomain+=ord(partByte).to_bytes(1,byteorder="big")



        #4.1.2. Question section format

        #end of domain
        dnsQuestionDomain += (0).to_bytes(1, byteorder="big")
        #type
        dnsQuestionDomain += (1).to_bytes(2, byteorder="big")

        #class
        dnsQuestionDomain += (1).to_bytes(2, byteorder="big")

        #
        #
        # print(dnsQuestionDomain)
        # exit(0)
        return dnsQuestionDomain


def handleClient(data,addr):

    print("(" + str(addr) + ") sent dns query")
    r = buildResponse(data)
    sock.sendto(r, addr)

while True:
    try:
        data, addr = sock.recvfrom(512)
        print("********** QUERY START **********")
        newThread = threading.Thread(target=handleClient,args=(data,addr))
        newThread.start()
        print("********** QUERY END **********")
    except:
        print("NO DATA ON SOCKET")
