# RUN

sudo python3 server.py

# NOTES

only specified domain - ip pairs will be poisoned otherwise your domain request will be resolve via os's dns server <br>

# POISON EXAMPLE


script will search a file named poison_list.txt (file name is hardcoded) and find test.com, then it will return the ip you specified,
other domains will be resolve with you os's dns, check os-dns-test.py

### POISON_LIST.TXT FORMAT

test.com 127.1.1.1  <br>
test1.com 127.2.3.4 <br>
test2.com 127.4.5.6 <br>
test3.com 127.7.8.9 <br>

as you can see there is no test4.com so test4.com will resolve via os's dns resolver but test3.com will return 127.7.8.9 in absolute level


# CREDIT

https://www.youtube.com/playlist?list=PLBOh8f9FoHHhvO5e5HF_6mYvtZegobYX2